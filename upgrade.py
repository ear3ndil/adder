#!/usr/bin/env python
## -*- coding: latin1 -*-
#Instalador de paquetes adder
#Por Jordi Ivars, 2004
#jordi@ultimobyte.es

import sys
from ftplib import FTP
import ConfigParser
import os
import string
import pickle
import install

config = ConfigParser.ConfigParser()
config.add_section("server")
config.add_section("users")

conf_file = open("/etc/adder/adder.conf","r")
config.readfp(conf_file)

servidor=config.get("server","ftp_server")
rama=config.get("server","rama")

sys.path.append('/var/cache/adder/upgrades')
sys.path.append('/usr/share/adder')

import mensajes

def importa(arch):
		import imp
		d=imp.find_module(arch)
		return imp.load_module(arch,*d)

class actualiza:

	def __init__(self):
	
		import adder_ftp
		import upgrade
		import dircache
		import string
		sys.path.append('/var/cache/adder/upgrades')
	
		#Averiguar cuantas instalaciones tenemos instaladas	
		directorio=dircache.listdir('/var/cache/adder/upgrades')		
		local=len(directorio)
		
		os.system("rm /var/cache/adder/upgrades/*pyc")
		
		#Mensaje: Cabecera
		print mensajes.cabeza
		
		#Mensaje:Conexión de actualizacion
		print mensajes.conexion_upgrade

				
		if rama == "inestable":
		
			#Mensaje: Rama inestable no tiene upgrades
			print mensajes.upgrade_inestable
			sys.exit()
			
		else:
			
			#Primera comprobación de actualizaciones:
			
			remoto=local+1
			
			try:
				self.comprueba(remoto)
			except:
				
				#Mensaje: No hay actualizaciones disponibles
				print mensajes.no_upgrade2
				sys.exit()
			
			#Variables tontas para crear un bucle infinito
		
			a=1
			b=2
				
			while a < b:
				try:
					self.comprueba(remoto)
				except:
					break
			
				try:
					self.instala(remoto)
					self.instala_update(remoto)
					remoto=remoto+1
				except:
					#Mensaje: Actualización completada
					print mensajes.upgrade_ok
					break

			
			
######
#Comprueba si existen actualizaciones
######

	def comprueba(self,remoto):
		import adder_ftp
		programa='update'+str(remoto)+'.py'
			
		adder_ftp.comprobar(programa)
			
	
	def instala(self,remoto):
	
		import adder_ftp
		
		programa='update'+str(remoto)+'.py'
					
		adder_ftp.actualizar(programa)
				

	def instala_update(self,remoto):
		
		argsz=remoto-1
		programa='update'+str(remoto)
				
		camino='/var/cache/adder/upgrades/'+programa
		la=importa(programa)
		la2=la.upgrade
		z=len(la2)-1
		x=0
			
		#Aquí va la instalació del programa
		
		while x <= z:
			
			valor=la2[x]
			
		#Buscar algun métode d'instalació, a poder ser sense gastar system. De moment
		#gastem system (hi ha problemes amb ftp.quit)
		
		#Crear métode que diga que si el programa no está instalat, no s'actualitza
			
			if self.comprobar_instalado(valor)=="si":
				os.system("adder install %s" % valor)
			else:
				val=valor.split("-")
			
				#Mensaje: Programa no instalado, no se actualiza
				print mensajes.cabeza2+val[0]+mensajes.no_upgrade3

			x=x+1

	
	def comprobar_instalado(self,valor):
		
		import dircache
		lista=dircache.listdir('/usr/local/var/adder/db')
		val=valor.split('-')
	
		coincide="no"
		b2=0
		
		for x in lista:
		
			a=0
			c=x.split("-")
			if c[a] == val[0]:
				coincide="si"
				lugar=b2
			b2=b2+1
			
		return coincide
