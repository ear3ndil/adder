#!/usr/bin/env python
## -*- coding: latin1 -*-
#Instalador de paquetes adder
#Por Jordi Ivars, 2004, 2009
#jordi@ultimobyte.es

import sys
import os
	
sys.path.append('/usr/share/adder')

class adder:

	def __init__(self):
	
#Ordenes a adder

		if len(sys.argv) == 1:		
	
			import adderhelp
			adderhelp.ayuda(None)
			sys.exit()

		if sys.argv[1] == "clear":
	
			self.clear()
	
		if sys.argv[1] == "del":

			self.delete(sys.argv[2])
	
		if sys.argv[1] == "install":
			
			self.install(sys.argv[2])
			
		if sys.argv[1] == "list":
			
			self.listar()
		
		if sys.argv[1] == "search":
			
			self.buscar(sys.argv[2])
				
		if sys.argv[1] == "show":
			
			self.mostrar(sys.argv[2])
							

		if sys.argv[1] == "upgrade":
		
			self.actualizar()	


		if sys.argv[1] == "help":
	
			longitud = len(sys.argv)
		
			if (longitud == 2): 

				import adderhelp
				adderhelp.ayuda(None)
			elif (longitud == 3):
					
				import adderhelp				
				adderhelp.ayuda(sys.argv[2])
			else: 
					
				import mensajes
				print mensajes.error_ayuda

#Argumentos a adder

		if sys.argv[1] == "-h":
		
			import adderhelp
			adderhelp.ayuda()	
			
		if sys.argv[1] == "-v":
				
			import adderhelp
			adderhelp.version()
			
			
####
#Limpiar paquetes
####
		
	def clear(self):
	
		import clear
		clear.limpiar()
	
#####
#Borrar paquetes
#####

	def delete(self,programa):
	
				
		import delete
		delete.borrado(programa)
		

####
#Instalar paquetes
####


	def install(self,programa):
	
		
		import install
		install.instalar(programa)
		
		
#####
#Listar paquetes
#####
	
	def listar(self):	
		
		import list
		list.listar()

#####
#Buscar paquetes
#####

	def buscar(self,programa):
		
		import search
		search.buscar(programa)
		
###
#Mostrar paquetes instalados
###
	def mostrar(self,programa):

		import show
		show.mostrar(programa)

######
#Actualización de paquetes
######

		
	def actualizar(self):	

		import upgrade
		upgrade.actualiza()	
		

#try:

adder()
	
#except:
	
#	print "--Instalador de paquetes <adder>--"
#	print " "
#	print """Ha ocurrido un error. Consulta la ayuda de adder con adder -h
#y asegúrate que la configuración del archivo adder.conf es la	
#orrecta."""
