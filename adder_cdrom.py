#!/usr/bin/env python
## -*- coding: latin1 -*-
#Instalador de paquetes adder
#Por Jordi Ivars, 2004
#jordi@ultimobyte.es

import sys
import ConfigParser

config = ConfigParser.ConfigParser()
config.add_section("server")
config.add_section("users")

conf_file = open("/etc/adder/adder.conf","r")
config.readfp(conf_file)

servidor=config.get("server","ftp_server")
rama=config.get("server","rama")

def instalacion(programa):

	os.system("cp /mnt/cdrom/"+programa+".bined.tgz /var/cache/adder/files/")

def listar():

	os.system("cp /mnt/cdrom/pak.db /var/cache/adder")
