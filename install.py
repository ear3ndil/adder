#!/usr/bin/env python
## -*- coding: latin1 -*-
#Instalador de paquetes adder
#Por Jordi Ivars, 2004, 2009
#jordi@ultimobyte.es

import sys
import os
from os.path import exists
import ConfigParser
import adder_ftp
import dircache
from time import strftime


config = ConfigParser.ConfigParser()
config.add_section("server")
config.add_section("users")
conf_file = open("/etc/adder/adder.conf","r")
config.readfp(conf_file)

usuario=config.get("users","user")
contras=config.get("users","password")
servidor=config.get("server","ftp_server")
rama=config.get("server", "rama")
		
sys.path.append('/usr/share/adder')
import mensajes

class instalar:

	def __init__(self,programa):
		
		#Ara comprova el que hi ha instalat amb el ftp i instala la versio
		#superior. Falta que comprove tambe les subversions que ara no trova.
		
		#Mensaje: Cabecera
		print mensajes.cabeza
		
		instalados=dircache.listdir('/usr/local/var/adder/db')
		
		programax=programa.split("-")
		
		subversion_ftp="no"
		subversion_local="no"
		ftp_i_local=[]
		#print programax		
		
		if len(programax) == 1:
			instalado="no"
			for x in instalados:
				
		#Afegim la versio local a una llista
		
				if x.split("-")[0] == programa:
		
		#Aqui comprovem si localment ja existeix. Si existeix, instalat=si		
					ftp_i_local.append(x.split("-")[1])
					instalado="si"
					if len(x.split("-")) == 3:
						subversion_local="si"
						subversiondata_local=x.split("-")[2]			
			
		elif exists ("/usr/local/var/adder/"+programa):
		
			instalado = "si"
			
					
		else:
			
			instalado = "no"
		
		
		#print "Subversion %s" % subversion_local
		#print "Instalado %s" % instalado
		if instalado == "si":
		
			ftp_remot=adder_ftp.existe(programa)	
			
		#Afegim la versio remota a una llista
			ftp_prova=[]
			for x in ftp_remot:
			
				if x.split("-")[0] == programa:
							
					a=x.split("-")[1]
					ftp_i_local.append(a.split(".bined.tgz")[0])
					ftp_prova.append(x)	
				
#Aqui tenemos la subversion del ftp

					if len(x.split("-")) == 3:
						subversion_ftp="si"
						#subversiondata_ftp=a.split("-")[1]						
						b=x.split("-")[2]
						#print b.split(".")[0]
			
			index=len(ftp_i_local)
			
			#print subversion_ftp
			#print subversiondata_ftp
			#print ftp_i_local
			#print ftp_prova
			
			if index > 1:		
		
		#Aqui tenim la versió superior entre local i ftp
				programa=programa+"-"+ftp_i_local[index-1]
				self.ftp(programa)

			else:
				sys.exit()
	
		elif servidor == "cdrom":
	
				self.cdrom(programa)

		else:
				self.ftp(programa)
	
	
	def cdrom(self,programa):
		
		
		#Mensaje: Instalación desde cdrom
		print mensajes.instala_cdrom
			
		adder_cdrom.instalacion(programa)
		
		os.system("tar xfz /var/cache/adder/files/"+programa+".bined.tgz"+" "+"-C /")
			
		if exists ("/usr/local/var/adder/"+programa):

			os.system("sh /usr/local/var/adder/"+programa)
			os.system("rm /usr/local/var/adder/"+programa)

		else:
			
		#Mensaje: Instalación con éxito #-#
			print mensajes.exito
		
			sys.exit()

	
	def ftp(self,programa):
	
	
		#Comprobació del nom de paquet. Hi ha que descarregar una llista
		#de tots els paquets disponibles i comparar la cadena de text donada
		#amb totes elles. Abans, hi ha que veure si la cadena donada es d'una part
		#o de dos (nom o nom+version)
	
		#¿Gastar adder_ftp.existe
		#Entrada nom o nom+versio
		#Sortida nom+versio+tgz
	
		#Mensaje:Conexión 
		print mensajes.conexion+programa
				
				
		llista=adder_ftp.existe(programa)
		
		
		#Del programa donat, buscar si te nom o nom+versio
		
		#Dividim el programa en parts per - 0=nom, 1=versio si la te, 2=subversio
		
		pograma = programa.split("-")
						
		#Comprobem si el programa existeix
					
		a=0
		b=0
		posicion=[]
		coincide = "no"
		while a < len(llista):
		
			lala=llista[a].split("-")
			
		#Busquem la coincidencia recorrent la llista
			
			for x in lala:
						
				c=a
				if x == pograma[0]:
					coincide="si"
					#Guardamos la posicion de la coincidencia en una lista	
					posicion.append(a)
					b=b+1
					
			a=a+1		
					
		
		#Instalacion cuando solo hay UNA version.
		if len(pograma) == 1:	
			
			if coincide == "no":
			
				#Mensaje: Error		
				print mensajes.error
				sys.exit()
		
			elif coincide == "si":

				if len(posicion) == 1:
				
					#Si solo hay una version del programa, se instala sin mas calculos
					
					#Sacamos el nombre del programa completo sacandolo de la lista 
					#y la posicion calculada anteriormente					
					prog=llista[int(posicion[0])]
					
					#Asimilamos el nombre de programa completo al valor programa.
					#El split es para guardar compatibilidad en el modulo de ftp, se podria
					#eliminar aqui y alli.
					
					programa=prog.split('.bined')
					programa=programa[0]
					self.post_install(programa)			
							
				elif len(posicion) > 1:
				
						
					#Sacamos todas las versiones+bined.tgz
					
					versiones=[]
					for x in posicion:
											
						versiones.append(llista[x])
		
					#versiones=llista nom+version+binedtgz
										
					#Hem de llevar els binedtgz dels noms+versio
					
					a=1
					b=1
					vers=[]
					while len(versiones) >= b:
					
						vers.append(versiones[len(versiones)-a].split(".bined")[0])
						b=b+1
						a=a+1
					
					#vers=llista amb nom+versio completa					
								
					#Ara hem de deixar una llista solament amb les versiones (el mateix split de dalt)
					#Hem d'anar amb compte amb les subversions
					
					a=1
					b=1
					vers2=[]
					while len(vers) >= b:
					
						vers2.append(vers[len(vers)-a].split("-")[1])
						b=b+1
						a=a+1
					
					#tenim una llista (vers2) solament amb les versions principals. Ara hem de
					#recorrer tots els numeros un a un per determinar el major, com fem
					#mes endavant per a comprovar si es major el que instalem que el
					#instalat
							
				
					#Volcar la llista original a una copia abans de afegir els 0
					
					vers3=[]
					
					for x in vers2:
					
						vers3.append(x)
						
					#Amb aixo entre als distints valors de la llista
					
					
					voltes=len(vers2)
					
					
					#Omplim tots els continguts de les versions a 3 caracters si fa falta
					#afegint un 0 al final (maxim 3)
					a=0
					while a < voltes:
					
						if len(vers2[a].split(".")) < 3:
							vers2[a]=vers2[a]+".0"

						a=a+1						
					
				
					#En teoria, la llista esta ordenada i la última versió
					#pareix ser sempre la mes gran
					
					
					programa=programa+"-"+vers3[voltes-1]
					version=vers2[voltes-1]
					
					a=0
									
					for x in vers2:
					
						if x == version:
						
							a=a+1
	
					if a > 1:

					
						#Veure les repeticions de programa i detectar la subversio
					
						prova=[]
						for x in vers:

							if len(x.split("-")) == 3:
								
								prova.append(x.split("-"))
								
												
						
						programa = prova[0]
						programa = programa[0]+"-"+programa[1]+"-"+programa[2]
						
						
						self.post_install(programa)
						
			#No se si el else es necessari. 
			
					#else:
					
					#	print "Instalar el programa"+programa
		
	
####Instalacion de programa+version. Facil###COMPLETADO####
		
		elif len(pograma) > 1:
		
			#Buscar en la llista de noms+versio
			
			if coincide == "no":
								
				#Mensaje: Error		
				print mensajes.error			
				sys.exit()
			
			elif coincide == "si":
							
		#Hasta aqui se ha comprobado el nombre. Queda comprobar nombre+version
		
				error="si"
				for x in llista:
					if x==programa+".bined.tgz":
						self.post_install(programa)		
						error="no"
			
				if error=="si":
				
					#Mensaje: Error		
					print mensajes.error	
			
		
		#print "Saliendo"
		#sys.exit()
		#self.post_install(programa)
		
	
	def post_install(self,programa):

		
		a=programa.split("-")
		b=a[1].split(".")
		nombre=a[0]
		
		if len(a) == 3:
	#Número de versión de actalizaciones propias, si existen.
	
			versionpropia=a[2]
		
		else:
		
			versionpropia="no"
		
		c=2
		d=len(b)
		e=d-c
		
		while len(b)< 3:
		
			b[c:c]=[0]
			e=e+1
						
		
		while len(b) > 3:
		
			b[3:d]=[]
			d=d-1
			
		
		import dircache
		lista=dircache.listdir('/usr/local/var/adder/db')
		
		coincide="no"
		b2=0
	

		for x in lista:
		
			a=0
			c=x.split("-")
		
			if c[a] == nombre:
				coincide="si"
				lugar=b2
						
			b2=b2+1
		
	
 	  	#Hasta aquí, coincide o no el nombre. Buscamos ahora coincidencia de versión
		#En b tenemos la lista de version de lo que se va a instalar
		#En lista[lugar] tenemos el programa ya instalado
		
		if coincide=="si":
			desinstala=lista[lugar]
			version=lista[lugar].split("-")
	
			#Chequeamos si el programa de instalacion tiene  numero de actualizacion
				
			
			if len(version) == 3:
		
				versionpropia2=version[2]
			else:
				versionpropia2="no"

			version=version[1].split(".")
				
		
			c=2
			d=len(version)
			e=d-c
		
			while len(version)< 3:
		
				version[c:c]=[0]
				e=e+1
							
		
			while len(version) > 3:
		
				version[3:d]=[]
				d=d-1
				
				
			if b[0] > version[0]:
			
				#Mensaje: Se va a actualizar
				print mensajes.si_upgrade+desinstala
				self.update_mod(programa,desinstala)
			
			elif b[0] < version[0]:
			
				#Mensaje: No se actualizará
				print mensajes.no_upgrade
							
				sys.exit()
			
			elif b[0] == version[0]:
			
				if b[1] > version[1]:
				
					#Mensaje: Se va a actualizar
					print mensajes.si_upgrade+desinstala
					self.update_mod(programa,desinstala)					
					
				elif b[1] < version[1]:
					
					#Mensaje: No se actualizará
					print mensajes.no_upgrade
					sys.exit()
			
				elif b[1] == version[1]:
					
					if b[2] > version[2]:
					
						#Mensaje: Se va a actualizar
						print mensajes.si_upgrade+desinstala
						self.update_mod(programa,desinstala)
											
					elif b[2] < version[2]:
					
						#Mensaje: No se actualizará
						print mensajes.no_upgrade
						sys.exit()
					
					elif b[2] == version[2]:
					
					#Mensaje: Programa ya instalado
					#Aqui va el chequeo de la version propia				
		
					#versionpropia=numero actualizacion a instalar
					#versionpropia2=numero actualizacion instalado
		
						if versionpropia != "no":
							
							if versionpropia2 == "no":
							
								#Mensaje: Se va a actualizar
								print mensajes.si_upgrade+desinstala
								self.update_mod(programa,desinstala)
								
							else:
							
								if versionpropia > versionpropia2:
								
								#Mensaje: Se va a actualizar
									print mensajes.si_upgrade+desinstala
									self.update_mod(programa,desinstala)
								
								elif versionpropia == versionpropia2:
								
									print mensajes.ya_instalado
									sys.exit()
			
						else:
			
							print mensajes.ya_instalado
							sys.exit()
			
		else:

			self.instalador(programa)		
		
	
	def update_mod(self,programa,desinstala):
		
		import delete
		delete.borrado(desinstala)
		self.instalador(programa)
			
	
	def instalador(self,programa):
		
		
		#Descargar el programa a instalar
		
		try:		
			adder_ftp.descargar(programa)
		except:
		
		 #Mensaje: Error		
		
			print mensajes.error
			
		#Mensaje: Ayuda
		
			print mensajes.ayuda
		
			sys.exit()
		
		
		#Mensaje: Proceso de instalación
		print mensajes.instalando+programa

		os.system("cd /var/cache/adder/files ; tar xfz"+" "+programa+".bined.tgz"+" "+"-C /")
		os.system("/sbin/ldconfig")
		#Mensaje: Instalación con exito
		print mensajes.exito
		
		#Guardar un log de la instalacion
		logfile="/var/log/adder/adder-install"
		fecha=strftime("%d/%m/%Y - %H:%M:%S")
		logvalor="Se ha instalado el programa "+programa+" en "+fecha+"\n"		

		f = open(logfile,"a")
		f.write(logvalor)
		f.close()
	
		if exists("/usr/local/var/adder/"+programa):
			os.system("sh /usr/local/var/adder/"+programa)
			os.system("rm /usr/local/var/adder/"+programa)
			
		else:
		
			#Mensaje: Instalación con exito
			#print mensajes.exito
			sys.exit()
		
		
		#Mensaje: Instalación con exito
		#print mensajes.exito
