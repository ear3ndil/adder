#!/bin/bash

rm -rf /usr/share/adder
rm /usr/bin/adder*
cp adder.py /usr/bin/adder
cp adder-* /usr/bin
mkdir /var/cache/adder
mkdir /var/cache/adder/files
mkdir /var/cache/adder/upgrades
mkdir /usr/local/var/adder
mkdir /usr/local/var/adder/db
mkdir /etc/adder
cp adder.conf /etc/adder
mkdir /usr/share/adder
cp *.py /usr/share/adder
