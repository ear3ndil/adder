#!/usr/bin/env python
## -*- coding: latin1 -*-
#Instalador de paquetes adder
#Por Jordi Ivars, 2004
#jordi@ultimobyte.es

import sys
import ConfigParser

config = ConfigParser.ConfigParser()
config.add_section("server")
config.add_section("users")
conf_file = open("/etc/adder/adder.conf","r")
config.readfp(conf_file)

usuario=config.get("users","user")
contras=config.get("users","password")
servidor=config.get("server","ftp_server")
rama=config.get("server", "rama")

cabeza2="adder>>> "

conexion="""adder>>> Usando el servidor ftp: %s
adder>>> Usando el usuario de acceso: %s
adder>>> Usando la rama: %s
adder>>> Comprobando el paquete """ % (config.get("server","ftp_server"),config.get("users","user"),rama)
			
conexion_upgrade="adder>>> Se va actualizar el sistema..."
			
error=cabeza2+"Ha habido un error. Posiblemente el paquete no exista o el servidor adder sea incorrecto o no esté disponible."
error_ayuda=cabeza2+"Ha habido un error. Los argumentos introducidos son incorrectos"

cabeza="adder>>>>>"
cabeza3=""

ya_instalado=cabeza2+"Este programa ya está instalado y no se vuelve a instalar."

instala_cdrom=cabeza2+"Instalando paquetes desde cdrom... Instalando..."

exito=cabeza2+"El programa ha sido instalado correctamente."
			
no_upgrade=cabeza2+"El programa no se va a instalar. Hay una version superior ya instalada."
			
si_upgrade=cabeza2+"Se actualizará desde "
			
instalando=cabeza2+"Instalando el paquete "

upgrade_inestable=cabeza2+"La rama inestable de adder no dispone de actualizaciones."

no_upgrade2=cabeza2+"No hay actualizaciones disponibles."

upgrade_ok=cabeza2+"La actualización ha sido completada con éxito."

no_upgrade3=": El programa no está instalado y no se actualiza."

borrando=cabeza2+"Borrando paquete..."

borrado_ok=cabeza2+"El paquete ha sido borrado con éxito."

borrado_no=cabeza2+"Ha ocurrido un error. El nombre de paquete introducido no está instalado."

ayuda=cabeza2+"Usa el comando adder help para consultar la ayuda disponible."

listar=cabeza2+"Listando todos los paquetes disponibles..."

limpiar=cabeza2+"Limpiando los paquetes descargados anteriormente..."

limpiar_ok=cabeza2+"Los paquetes han sido eliminados con éxito."

listar2=cabeza2+"Listando todos los paquetes instalados..."
