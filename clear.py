#!/usr/bin/env python
## -*- coding: latin1 -*-
#Instalador de paquetes adder
#Por Jordi Ivars, 2004
#jordi@ultimobyte.es

import sys
import os

sys.path.append('/usr/share/adder')
import mensajes

def limpiar():

	#Mensaje: Cabecera
	print mensajes.cabeza
	
	#Mensajes: Limpiando
	print mensajes.limpiar
	
	os.system("rm /var/cache/adder/files/*")
	
	#Mensajes: Limpiar ok
	print mensajes.limpiar_ok
