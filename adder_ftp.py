#!/usr/bin/env python
## -*- coding: latin1 -*-
#Instalador de paquetes adder
#Por Jordi Ivars, 2004
#jordi@ultimobyte.es

import sys
from ftplib import FTP
import ConfigParser

config = ConfigParser.ConfigParser()
config.add_section("server")
config.add_section("users")

conf_file = open("/etc/adder/adder.conf","r")
config.readfp(conf_file)

servidor=config.get("server","ftp_server")
rama=config.get("server","rama")

usuario=config.get("users","user")
contras=config.get("users","password")

ftp=FTP(config.get("server","ftp_server"))
ftp.login(usuario,contras)

if rama == "estable": 

	ftp.cwd("adder/estable")
elif rama == "inestable":
				
	ftp.cwd("adder/inestable")


def listar():

		ftp.retrbinary('RETR pak.db',open('/var/cache/adder/pak.db','wb').write)
		ftp.quit()


def actualizar(programa):

		if rama == "inestable":
		
			print "La rama inestable de adder no dispone de actualizaciones"
			sys.exit()
			
		else:
		
			
			ftp.cwd("/adder/estable/upgrades/")
			ftp.retrbinary('RETR '+programa,open('/var/cache/adder/upgrades/'+programa,'wb').write)
			
			#Si cerramos el ftp funciona mal el update por el bucle desde upgrade.py
			#ftp.quit()
			
def comprobar(programa):

		ftp.cwd("/adder/estable/upgrades")
		#Buscamos el tamaño del archivo a descargar. Si no existe el archivo, dara error.
		ftp.size(programa)
		#ftp.quit()
		
def existe(programa):

		
	if rama == "estable": 

		ftp.cwd("/adder/estable")
	
	elif rama == "inestable":
				
		ftp.cwd("/adder/inestable")
	

	#Tenim una llista amb tots els programes i directoris del ftp
	llista = ftp.nlst()

	return llista

	#ftp.quit()		
		

def descargar(programa):
	
	if rama == "estable": 

		ftp.cwd("/adder/estable")
	
	elif rama == "inestable":
				
		ftp.cwd("/adder/inestable")
		
	ftp.retrbinary('RETR '+programa+".bined.tgz",open('/var/cache/adder/files/'+programa+".bined.tgz",'wb').write)
		#ftp.retrbinary('RETR deps.py',open('/var/cache/adder/deps.py','wb').write)
	ftp.quit()
