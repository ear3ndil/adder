#!/usr/bin/env python
## -*- coding: latin1 -*-
#Instalador de paquetes adder
#Por Jordi Ivars, 2004, 2009
#jordi@ultimobyte.es


#Ayuda general para todo el adder

#fecha="21/12/2004"
fecha="13/1/2009"
version="0.6.2.2"

import sys
import os

class ayuda:

	def __init__(self,arg1):	
#
#Se llama a help sin argumentos.
#		
		if (arg1 == None):  				
			
			version="0.6.2.2"
			
			print " "
			print "Adder, versi�n "+version
			print " "
			print "Herramienta utilizada para manejar distintos programas en nuestro sistema."
			print "Podemos instalar, desinstalar, ver y buscar programas."
			print "Sintaxis:  " + "\n"
			print "adder orden nombre_paquete-versi�n. Por ejemplo adder install mplayer_pre-1.0.5"+ "\n"
			print "Las �rdenes posibles son: " + "\n\n" "install  del  search  show  list  clear  upgrade "+ "\n"
			print "Si deseamos consultar ayuda espec�fica sobre una orden teclear: " + "\n" + "adder help       -> Muestra este mensaje. " + "\n" + "adder help orden -> Muestra la ayuda asociada a esa orden" + "\n"
			print "Para saber qu� versi�n de adder estamos utilizando, ejecutar 'adder help version'."+ "\n"	
		
#			
# Se ha llamado a help con algun parametro. 
#	
		elif (arg1 == "clear"):	self.clear_ayuda()
	
		elif (arg1 == "del"): self.delete_ayuda()
	
		elif (arg1 == "install"): self.install_ayuda()
			
		elif (arg1 == "list"):	self.listar_ayuda()
		
		elif (arg1 == "search"): self.buscar_ayuda()
				
		elif (arg1 == "show"):	self.mostrar_ayuda()
							
		elif (arg1 == "upgrade"): self.actualizar_ayuda()
		
		elif (arg1 == "version"): self.version_ayuda()

#
# El parametro pasado es erroneo.
#		
		else: print "Ha habido un error. Has tecleado una orden que no existe... "
		
#
# Una funcion por cada orden posible.
#
	def clear_ayuda(self):
		
		print " "
		print "Adder, versi�n "+version
		print " "
		print "Ayuda para el comando :  " + "clear"+ "\n"
		
		print "Ejecutando el mandato 'adder clear' borramos todos los paquetes que han sido descargados mediante la orden 'adder install'."
		print "El programa los deja en un directorio local. La ruta del directorio es :  "+ "/var/cache/adder/files/" + "\n"
		print "Para m�s informaci�n sobre el proceso de instalaci�n ejecutar: "
		print "'adder help install'" + "\n"		
	
	def delete_ayuda(self):
	
		print " "
		print "Adder, versi�n "+version
		print " "
		print "Ayuda para el comando : " + "  del  "+ "\n"
		print "La orden 'del' nos permite borrar un paquete que ya est� instalado."
		print "El nombre del paquete a borrar se le pasa a adder como par�metro 'adder del nombre_paquete'" + "\n"
		print "Para obtener informaci�n sobre todos los paquetes instalados, ejecutar :"
		print "'adder show all'"+ "\n"
			
	def install_ayuda(self):
		
		print " "
		print "Adder, versi�n "+version
		print " "
		print "Ayuda para el comando : "+ " install "+ "\n"
		print "Para instalar un paquete determinado ejecutar 'adder install nombre_paquete'."
		print "El nombre del paquete a instalar se le pasa a adder como par�metro 'adder install nombre_paquete'."
		print "Si se quiere obtener una lista con los paquetes disponibles, ejecutar 'adder list'."
		print "Imprescindible que el nombre del paquete a instalar sea introducido exactamente como est� en la lista." + "\n"
		print "Si el nombre introducido es correcto, adder se descargar� el paquete junto con todas sus dependencias."
		print "Una vez instalado, el paquete descargado se queda en un directorio."
		print "Si se desean limpiar esos ficheros, ejecutar 'adder clear'."
		print "Para obtener informaci�n sobre la limpieza de paquetes instalados, ejecutar :"
		print "'adder help clear'"+ "\n"
	
	def listar_ayuda(self):
		
		print " "
		print "Adder, versi�n "+version
		print " "
		print "Ayuda para el comando : "+ " list" + "\n"
		print "Para obtener una lista de todos los paquetes disponibles, ejecutar 'adder list'"
		print "Los paquetes pueden estar en un servidor ftp, o bien en alguna unidad de cd-rom."
		print "Esta lista de archivos se guarda en un archivo llamado 'pak.db' que se encuentra en: "
		print "/var/cache/adder/"+ "\n"
		
	def buscar_ayuda(self):
		
		print " "
		print "Adder, versi�n "+version
		print " "
		print "Ayuda para el comando : " + " search " + "\n"
		print "Para buscar un paquete determinado en la lista de paquetes disponibles, ejecutar 'adder search nombre_paquete'."
		print "M�s informaci�n sobre dicha lista mediante 'adder help list'."+ "\n"
		print "No es necesario que el nombre del paquete a buscar sea introducido tal y como es."
		print "La funci�n search busca todos los paquetes que contengan la cadena que le pasemos como par�metro."
		print "Por ejemplo, 'adder search kde' nos devolver� todos los paquetes del kde."
		print "No funciona sim par�metros; obligatorio pasarle una cadena." +"\n"
		print "Para obtener informaci�n sobre los paquetes disponibles ejecutar: "
		print "'adder list'" + "\n"
		print "Para obtener informaci�n sobre los paquetes instalados, ejecutar : "
		print "'adder show all'" + "\n"
		
	def mostrar_ayuda(self):
		
		print " "
		print "Adder, versi�n "+version
		print " "
		print "Ayuda para el comando : " + " show " + "\n"
		print "Para obtener una lista de los paquetes que tenemos instalados en el sistema ejecutar 'adder show all'."
		print "Se le puede decir a 'adder show' que muestre los paquetes instalados que empiecen por una cadena." + "\n"
		print "Para ello, le pasaremos en lugar de 'all' una cadena cualquiera."
		print "En este caso, adder nos devolver� todos los paquetes instalados cuyo nombre empiece por dicha cadena."
		print "Por ejemplo, 'adder show g' nos devolver� el nombre de aquellos programas instalados que empiecen por 'g'." + "\n"
		
	def actualizar_ayuda(self):
	
		print " "
		print "Adder, versi�n "+version
		print " "
		print "Ayuda para el comando : " + " upgrade " + "\n"
		print "Para actualizar todos los paquetes, utilizar 'adder upgrade'."+ "\n"
		
	def version_ayuda(self):
		
		print "adder, versi�n "+version
		print "Publicada en "+fecha + "\n"
