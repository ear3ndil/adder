#!/usr/bin/env python
## -*- coding: latin1 -*-
#Instalador de paquetes adder
#Por Jordi Ivars, 2004,2009
#jordi@ultimobyte.es

import sys
import os
from os.path import exists
from time import strftime

sys.path.append('/usr/share/adder')
import mensajes

def borrado(programa):

	#Mensaje: Cabecera
	print mensajes.cabeza
	#Mensaje: Borrando
	print mensajes.borrando

	pak=programa
	ruta='/usr/local/var/adder/db'
	todo=os.path.join(ruta,pak)

	if exists (todo):

		os.system("sh"+" "+todo+" "+"2&>/dev/null")
		
		#Mensaje: Borrado con éxito
		print mensajes.borrado_ok
		#Guardar un log del borrado
		logfile="/var/log/adder/adder-delete"
		fecha=strftime("%d/%m/%Y - %H:%M:%S")
		logvalor="Se ha borrado el programa "+programa+" en "+fecha+"\n"	

		f = open(logfile,"a")
		f.write(logvalor)
		f.close()

	else:
	
		#Mensaje: Error, paquete no está instalado
		print mensajes.borrado_no
		
		#Mensaje: Ayuda
		print mensajes.ayuda
